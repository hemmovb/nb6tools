# usbboot

You will need this to mount NB6 device when connecting it through USB.
Make sure the switch on the board is set to Enabled.

Run as sudo (sudo ./rpiboot).

Reconnect power to NB6 if nothing happens first.

# portablerecording
Prerequisites: Default Ansible roles should be installed already (nsw_utils, npjack etc).

A simple script for NB6 to record files manually. Press the keys as indicated on the menu to record and to stop recording.

## Installation

__Copy to NB6:__
scp portrec dev-10123:/home/nsw

__Make sure it has executable permissions:__
chmod +x portrec

# samplerate

Simple script to extract sample rate setting from all devices to csv format.

__Run:__
./ratealldev > sampleratelist.txt

